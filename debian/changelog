golang-github-mesos-mesos-go (0.0.6+dfsg-2) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Set debhelper-compat version in Build-Depends.
    Fixes lintian: uses-debhelper-compat-file
    See https://lintian.debian.org/tags/uses-debhelper-compat-file.html for more details.
  * Remove patches offline.patch that are missing from debian/patches/series.
    Fixes lintian: patch-file-present-but-not-mentioned-in-series
    See https://lintian.debian.org/tags/patch-file-present-but-not-mentioned-in-series.html for more details.

  [ Shengjing Zhu ]
  * Update maintainer address to team+pkg-go@tracker.debian.org
  * Replace golang-go with golang-any.
    And Remove golang-go from -dev Depends
  * Update Standards-Version to 4.5.0 (no changes)

 -- Shengjing Zhu <zhsj@debian.org>  Tue, 11 Feb 2020 14:32:46 +0800

golang-github-mesos-mesos-go (0.0.6+dfsg-1) unstable; urgency=medium

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Dmitry Smirnov ]
  * New upstream release.
  * Testsuite: autopkgtest-pkg-go
  * Priority: optional; Standards-Version: 4.4.1
  * DH & compat to version 12.
  * (Build-)Depends += "golang-github-pquerna-ffjson-dev".
  * rules: DH_GOLANG_GO_GENERATE := 1

 -- Dmitry Smirnov <onlyjob@debian.org>  Wed, 16 Oct 2019 12:53:28 +1100

golang-github-mesos-mesos-go (0.0.2+dfsg-3) unstable; urgency=medium

  [ Tim Potter ]
  * Team upload.
  * Add me to uploaders

  [ Michael Stapelberg ]
  * Switch to XS-Go-Import-Path

 -- Michael Stapelberg <stapelberg@debian.org>  Fri, 09 Feb 2018 09:32:56 +0100

golang-github-mesos-mesos-go (0.0.2+dfsg-2) unstable; urgency=medium

  [ Paul Tagliamonte ]
  * Use a secure transport for the Vcs-Git and Vcs-Browser URL

  [ Dmitry Smirnov ]
  * New "offline.patch" to disable networking tests (Closes: #830675)
  * rules: do not ignore failing tests.
  * (Build-)Depends: removed transitional alternatives.
  * Standards-Version: 3.9.8
  * Removed Built-Using field.

 -- Dmitry Smirnov <onlyjob@debian.org>  Thu, 14 Jul 2016 23:46:54 +1000

golang-github-mesos-mesos-go (0.0.2+dfsg-1) unstable; urgency=medium

  * New upstream release [October 2015].
  * (Build-)Depends:
    - golang-testify-dev | golang-github-stretchr-testify-dev
    + golang-github-stretchr-testify-dev

 -- Dmitry Smirnov <onlyjob@debian.org>  Thu, 03 Mar 2016 23:40:20 +1100

golang-github-mesos-mesos-go (0.0.1+dfsg-1) unstable; urgency=medium

  * New upstream release [September 2015].
  * Build-Depends += "golang-objx-dev".
  * rules: added "DH_GOLANG_EXCLUDES = examples".

 -- Dmitry Smirnov <onlyjob@debian.org>  Tue, 29 Sep 2015 13:05:23 +1000

golang-github-mesos-mesos-go (0.0~git20150903.0.d98afa6-1) unstable; urgency=medium

  * Initial release (Closes: #798129).

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 06 Sep 2015 01:11:41 +1000
